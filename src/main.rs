mod account_manager;

use std::env;
use frankenstein::{Api, Chat, ChatId, ChatType, Error, GetUpdatesParams, MethodResponse, SendMessageParams, TelegramApi, Update, UpdateContent};
use frankenstein::AllowedUpdate::Message;
use crate::account_manager::AccountManager;


pub struct RpcConf {
    name: String,
    url: String
}

impl RpcConf {
    pub fn new(&self, name: String, url: String) -> Self {
        RpcConf {
            name,
            url
        }
    }

    pub fn get_name(&self) -> String {
        return self.name.clone()
    }

    pub fn get_url(&self) -> String {
        return self.url.clone()
    }
}

fn dummy_method_response() -> MethodResponse<frankenstein::Message> {
    return MethodResponse {
        ok: false,
        result: frankenstein::Message {
            message_id: 0,
            message_thread_id: None,
            from: None,
            sender_chat: None,
            date: 0,
            chat: Box::new(Chat {
                id: 0,
                type_field: ChatType::Private,
                title: None,
                username: None,
                first_name: None,
                last_name: None,
                is_forum: None,
                photo: None,
                active_usernames: None,
                emoji_status_custom_emoji_id: None,
                emoji_status_expiration_date: None,
                bio: None,
                has_private_forwards: None,
                has_restricted_voice_and_video_messages: None,
                join_to_send_messages: None,
                join_by_request: None,
                description: None,
                invite_link: None,
                pinned_message: None,
                permissions: None,
                slow_mode_delay: None,
                message_auto_delete_time: None,
                has_aggressive_anti_spam_enabled: None,
                has_hidden_members: None,
                has_protected_content: None,
                sticker_set_name: None,
                can_set_sticker_set: None,
                linked_chat_id: None,
                location: None,
            }),
            forward_from: None,
            forward_from_chat: None,
            forward_from_message_id: None,
            forward_signature: None,
            forward_sender_name: None,
            forward_date: None,
            is_topic_message: None,
            is_automatic_forward: None,
            reply_to_message: None,
            via_bot: None,
            edit_date: None,
            has_protected_content: None,
            media_group_id: None,
            author_signature: None,
            text: None,
            entities: None,
            animation: None,
            audio: None,
            document: None,
            photo: None,
            sticker: None,
            story: None,
            video: None,
            video_note: None,
            voice: None,
            caption: None,
            caption_entities: None,
            has_media_spoiler: None,
            contact: None,
            dice: None,
            game: None,
            poll: None,
            venue: None,
            location: None,
            new_chat_members: None,
            left_chat_member: None,
            new_chat_title: None,
            new_chat_photo: None,
            delete_chat_photo: None,
            group_chat_created: None,
            supergroup_chat_created: None,
            channel_chat_created: None,
            message_auto_delete_timer_changed: None,
            migrate_to_chat_id: None,
            migrate_from_chat_id: None,
            pinned_message: None,
            invoice: None,
            successful_payment: None,
            user_shared: None,
            chat_shared: None,
            connected_website: None,
            write_access_allowed: None,
            passport_data: None,
            proximity_alert_triggered: None,
            forum_topic_created: None,
            forum_topic_edited: None,
            forum_topic_closed: None,
            forum_topic_reopened: None,
            general_forum_topic_hidden: None,
            general_forum_topic_unhidden: None,
            video_chat_started: None,
            video_chat_ended: None,
            video_chat_scheduled: None,
            video_chat_participants_invited: None,
            web_app_data: None,
            reply_markup: None,
        },
        description: None,
    }
}


fn main() {



    // // Initialize RPC Config
    // let rpc_devnet = RpcConf{
    //     name: String::from("devnet"),
    //     url: String::from("https://api.devnet.solana.com")
    // };
    // let rpc_testnet = RpcConf{
    //     name: String::from("testnet"),
    //     url: String::from("https://api.testnet.solana.com")
    // };
    // let rpc_mainnet = RpcConf{
    //     name: String::from("mainnet"),
    //     url: String::from("http://rpc.solscan.com")
    // };
    //
    //
    // let command_args: Vec<String> = env::args().collect();
    // if command_args.clone().into_iter().count() >= 2 {
    //     let mut command_args_iter = command_args.iter();
    //     command_args_iter.next();
    //     match command_args_iter.next() {
    //         None => {
    //             panic!("ERROR: No subcommand provided. Options are devnet, testnet and mainnet")
    //         }
    //         Some(value) => {
    //             let rpc: RpcConf;
    //             if *value == rpc_devnet.get_name() {
    //                 rpc = rpc_devnet
    //             }
    //             else if *value == rpc_testnet.get_name() {
    //                 rpc = rpc_testnet
    //             }
    //             else if *value == rpc_mainnet.get_name() {
    //                 rpc = rpc_mainnet
    //             } else {
    //                 panic!("ERROR: Unsupported subcommand. Options are devnet, testnet and mainnet")
    //             }
    //
    //             let account_manager = AccountManager::new(rpc);
    //             match account_manager.add_account("Lusajop", "EdmwgVXBAcCbTTH7zicCrDBZzVyRgZkAq92grf8aU1ap") {
    //                 None => {}
    //                 Some(response) => {
    //                     if response == 200 {
    //                         println!("LOG: Added account successfully!")
    //                     } else if response == 409 {
    //                         println!("LOG: Account already registered!")
    //                     }
    //                 }
    //             }
    //
    //             let balance = account_manager.get_account_balance("EdmwgVXBAcCbTTH7zicCrDBZzVyRgZkAq92grf8aU1ap");
    //             println!("Account EdmwgVXBAcCbTTH7zicCrDBZzVyRgZkAq92grf8aU1ap balance {balance}SOL")
    //
    //         }
    //     }
    // } else {
    //     panic!("ERROR: No subcommand provided. Options are devnet, testnet and mainnet")
    // }
    // println!("Args: {:?}", command_args);



    let bot_token = "6639451298:AAHOX70kMEyCJTLCc-yV_XZiwy4pS8xtLfs";
    let db_connection = sqlite::open("./bot.db");

    match db_connection {
        Ok(connection) => {
            let migration_queries = "CREATE TABLE IF NOT EXISTS chats (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                chat_id INTEGER,
                first_name TEXT
            ); CREATE TABLE IF NOT EXISTS messages (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                message_id INTEGER,
                is_replied BOOLEAN
            ); CREATE TABLE IF NOT EXISTS msg_snapshot (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                user_id INTEGER,
                message TEXT
            )";
            connection.execute(migration_queries).expect("ERROR: Failed to migrate tables.");



            // TELEGRAM BOT API
            let api = Api::new(bot_token);

            let update_params = GetUpdatesParams {
                offset: None,
                limit: None,
                timeout: None,
                allowed_updates: Some(vec![Message]),
            };

            loop {
                let result = api.get_updates(&update_params);
                match result {
                    Ok(response) => {
                        // println!("Response: {:#?}", response);
                        for update in response.result {
                            let content = update.content;

                            if let UpdateContent::Message(msg) = content {
                                // println!("Message: {:#?} \n\n\n", msg);
                                let from_user = msg.from.unwrap();

                                // Check if message is already replied
                                let query = "SELECT * from messages where message_id=?;".to_string();

                                let mut saved_messages = connection.prepare(query).unwrap().into_iter()
                                    .bind((1, msg.message_id as i64)).unwrap().map(|row| row.unwrap());

                                if let Some(saved_message) = saved_messages.next() {
                                    // println!("Saved message: {}", saved_message.read::<&str, _>("is_replied"))
                                    println!("Saved message: {}", saved_message.read::<i64, _>("is_replied"));
                                } else {

                                    let mut reply_params = SendMessageParams {
                                        chat_id: ChatId::Integer(msg.chat.id),
                                        message_thread_id: None,
                                        text: "".to_string(),
                                        parse_mode: None,
                                        entities: None,
                                        disable_web_page_preview: None,
                                        disable_notification: None,
                                        protect_content: None,
                                        reply_to_message_id: None,
                                        allow_sending_without_reply: None,
                                        reply_markup: None,
                                    };

                                    if let Some(message) = msg.text {
                                        reply_params.reply_to_message_id = Option::from(msg.message_id);
                                        let mut reply_result;
                                        match message.as_str() {
                                            "/start" => {
                                                let reply_message = format!("Hello @{}, Welcome to SOL Drop Tracking Bot. \n\nAdd wallet: /addWallet", from_user.first_name);
                                                reply_params.text = reply_message;
                                                reply_result = api.send_message(&reply_params);

                                            },
                                            "/addWallet" => {
                                                let reply_message = "Enter account address e.g EdmwgVXBAcCbTTH7zicCrDBZzVyRgZkAq92grf8aU1ax \n\nAdd wallet: /addWallet".to_string();
                                                reply_params.text = reply_message;
                                                reply_result = api.send_message(&reply_params);
                                            }
                                            _ => {
                                                reply_result = Ok(dummy_method_response())
                                            }
                                        }
                                        match reply_result {
                                            Ok(_) => {
                                                let query = format!("INSERT INTO messages values (null, {}, {})", msg.message_id, true);

                                                let update_status_query = format!("INSERT INTO msg_snapshot values (null, {}, ?)", from_user.id);
                                                if let Ok(_) = connection.prepare(update_status_query).unwrap().bind((1, message.as_str())) {
                                                    connection.execute(query).expect("ERROR: Failed to insert messages!");
                                                } else {
                                                    panic!("ERROR: Failed to set message snapshot!")
                                                }

                                            }
                                            Err(_) => {}
                                        }
                                    }

                                }

                            }
                        }
                    }
                    Err(error) => {
                        println!("Error: {error}")
                    }
                }
            }

        }
        Err(err) => {}
    }





}
