use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use std::str::FromStr;
use once_cell::sync::Lazy;
use solana_client::rpc_client::RpcClient;
use solana_sdk::pubkey::Pubkey;
use crate::RpcConf;

static ACCOUNTS_FILE_PATH: Lazy<String> = Lazy::new(|| {
    String::from("./accounts.txt")
});


pub struct AccountManager {
    rpc: RpcConf,
    rpc_client: RpcClient
}


impl AccountManager {
    pub fn new(rpc: RpcConf) -> Self {
        let rpc_client = RpcClient::new(rpc.url.clone());
        AccountManager {
            rpc,
            rpc_client
        }
    }

    pub fn add_account(&self, account_name: &str, account_address: &str) -> Option<u16>{

        if !self.is_account_registered(account_address.clone()) {
            let mut accounts = OpenOptions::new()
                .create(true)
                .append(true)
                .open(ACCOUNTS_FILE_PATH.as_str())
                .expect("Failed to open file");
            match writeln!(accounts, "{account_name}/{account_address}") {
                Ok(..) => {}
                Err(_) => {
                    panic!("ERROR: Failed to write account!")
                }
            }
            return Some(200)
        } else {
            return Some(409)
        }
    }


    pub fn get_account_balance(&self, account_address: &str) -> u64{
        let account_public_key = Pubkey::from_str(account_address.clone()).unwrap();
        match self.rpc_client.get_balance(&account_public_key) {
            Ok(balance) => {
                return balance/1000000000
            }
            Err(_) => {}
        }
        return 0
        // println!("LOG: Account {account_address} balance is {:?}", self.rpc_client.get_balance(&account_public_key));
    }


    // Utils
    fn is_account_registered(&self, account_address: &str) -> bool {
        let accounts = File::open(ACCOUNTS_FILE_PATH.as_str());
        match accounts {
            Ok(mut file) => {
                let mut contents = String::new();
                file.read_to_string(&mut contents).unwrap();

                return contents.contains(account_address)
            }
            Err(_) => {
            }
        }
        true
    }


}

